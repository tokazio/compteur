# Kafka server

We are not using zookeeper but Kraft.

## Installation

in /opt/kafka, (verifyi version number to get the latest)

wget https://apachemirror.wuchna.com/kafka/2.8.0/kafka_2.12-2.8.0.tgz
mkdir /opt/kafka && cd /opt/kafka tar -xvzf ~/kafka.tgz --strip 1 cd kafka_2.12-2.8.0

## Configuration

```
cd /opt/kafka/config
cp server.properties server1.properties
```

sudo nano /opt/kafka/config/server1.properties

```
broker.id=1
log.retention.hours=-1
delete.topic.enable = true
log.dirs=/home/kafka/logs
process.roles=controller,broker
node.id=1
inter.broker.listener.name=PLAINTEXT
controller.listener.names=CONTROLLER
listeners=PLAINTEXT://:9092,CONTROLLER://:19092
listener.security.protocol.map=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,SSL:SSL>
controller.quorum.voters=1@localhost:19092
advertised.listeners=PLAINTEXT://192.168.1.50:9092
```

Change the ip if needed

## Service

/etc/systemd/system/kafka.service

```
[Unit]
Description=Kafka service
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

StartLimitIntervalSec=500
StartLimitBurst=5

[Service]
Type=simple
User=root
ExecStart=/bin/sh -c '/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server1.properties >> /opt/kafka/kafka.log'
ExecStop=/opt/kafka/bin/kafka-server-stop.sh
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
```

To relaod service script (to do on each changes)

```
systemctl daemon-reload
```

## Service logs

```
tail -fn 1000 /opt/kafka/kafka.log
```

## kafka more heap

/!\ Minimum required is 256M

```
export KAFKA_HEAP_OPTS="-Xmx256M"
```

or

Edit /opt/kafka/bin/kafka-server-start.sh

```
export KAFKA_HEAP_OPTS="-Xmx256M"
...
java ...
```

## Kafka Logs folder (re-create)

```
mkdir /home/kafka/logs
cd /home/kafka/logs
/opt/kafka/bin/kafka-storage.sh random-uuid
>OPuu1AJvSFux1imCR5vsIQ
sudo /opt/kafka/bin/kafka-storage.sh format --cluster-id OPuu1AJvSFux1imCR5vsIQ --config /opt/kafka/config/server1.properties
>Formatting /home/kafka/logs
```
