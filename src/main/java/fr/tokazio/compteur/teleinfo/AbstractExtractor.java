package fr.tokazio.compteur.teleinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractExtractor<T> implements Extractor<T> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractExtractor.class);

    protected int len;

    protected AbstractExtractor(int len) {
        this.len = len;
    }

    @Override
    public T extract(String str) {
        LOGGER.debug("Extracting '" + str + "'...");
        if (str == null || str.isEmpty()) {
            return empty();
        }
        if (len < 0 || str.length() < len) {
            final StringBuilder sb = new StringBuilder(str);
            int missing = len - str.length();
            while (missing >= 0) {
                sb.append(" ");
                missing--;
            }
            return doExtract(sb.toString());
        }
        return doExtract(str.substring(0, len < 0 ? str.length() : len));
    }

    public abstract T doExtract(String str);

    public abstract T empty();
}