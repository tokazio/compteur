package fr.tokazio.compteur.teleinfo;

import gnu.io.*;
import io.smallrye.common.annotation.Blocking;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static fr.tokazio.compteur.teleinfo.Etiquette.*;

/**
 * need apt-get install librxtx-java
 * cp /usr/lib/jni/*.so /lib
 *
 * @link https://www.planete-domotique.com/blog/2010/03/30/la-teleinformation-edf/
 * @link https://github.com/lhuet/vertx-teleinfo-hard/blob/master/src/main/java/fr/lhuet/hard/teleinfo/TeleinfoHardwareWorkerVerticle.java
 */
@ApplicationScoped
class TeleInfoReader implements Closeable {

    @ConfigProperty(name = "tty", defaultValue = "/dev/ttyUSB0")
    String portName;

    @Inject
    Logger log;

    @Inject
    TeleInfoParser parser;

    private SerialPort serialPort;

    @PostConstruct
    void init() {
        // Workaround to force serial port detection on Linux
        System.setProperty("gnu.io.rxtx.SerialPorts", portName);
    }

    private InputStream open() throws NoSuchPortException, UnsupportedCommOperationException, IOException {
        if (serialPort == null) {
            final CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(portName);
            if (portId.isCurrentlyOwned()) {
                log.error("Error: Port " + portName + " is currently in use");
            } else {
                log.info("Opening port " + portId.getName());
            }
            // Port settings -> 1200 7E1
            try {
                serialPort = (SerialPort) portId.open(getClass().getName(), 2000);
                serialPort.setSerialPortParams(1200, SerialPort.DATABITS_7, SerialPort.STOPBITS_1, SerialPort.PARITY_EVEN);
            } catch (PortInUseException ex) {
                //déjà ouvert
            }
        }
        return serialPort.getInputStream();
    }

    @Blocking
    public TeleInfoData readOnce() throws IOException, UnsupportedCommOperationException, NoSuchPortException {
        log.debug("Reading teleinfo (1s max)...");
        try (final InputStreamReader reader = new InputStreamReader(open());
             final BufferedReader br = new BufferedReader(reader)) {
            List<String> trame = null;
            final long start = System.currentTimeMillis();
            while (System.currentTimeMillis() - start < 5000) {
                final String line = br.readLine();
                // A new teleinfo trame begin with '3' and '2' bytes
                if (line != null && !line.isEmpty() && line.codePointAt(0) == 3 && line.codePointAt(1) == 2) {
                    // Trame complete (null on starting)
                    if (trame != null) {
                        //trame complète ici
                        final Map<Etiquette, Object> parsed = parser.parse(trame);
                        log.debug("trame: " + parsed);
                        //transform to dataobj
                        final TeleInfoData data = new TeleInfoData();
                        data.setDate(new Date());
                        data.setIndexHeuresCreuses((long) parsed.get(HCHC));
                        data.setIndexHeuresPleines((long) parsed.get(HCHP));
                        data.setAmperes((int) parsed.get(IINST));
                        data.setKwatts((int) parsed.get(PAPP));
                        return data;
                    }
                    // New trame starting
                    trame = new ArrayList<>();
                } else {
                    if (trame != null) {
                        trame.add(line);
                    }
                }
            }
        }
        throw new IOException("Read timeout without full data");
    }

    @Override
    public void close() {
        if (serialPort != null) {
            serialPort.close();
        }
    }
}