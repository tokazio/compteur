package fr.tokazio.compteur.teleinfo;

interface Extractor<T> {

    T extract(String str);
}