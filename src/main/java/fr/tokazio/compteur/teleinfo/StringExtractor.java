package fr.tokazio.compteur.teleinfo;

class StringExtractor extends AbstractExtractor<String> {

    StringExtractor(int len) {
        super(len);
    }

    StringExtractor() {
        super(-1);
    }

    @Override
    public String doExtract(String str) {
        return str;
    }

    @Override
    public String empty() {
        return "";
    }
}