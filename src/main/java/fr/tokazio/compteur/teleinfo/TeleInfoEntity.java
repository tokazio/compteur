package fr.tokazio.compteur.teleinfo;

import javax.persistence.*;
import java.util.Date;

@Entity
@Cacheable
@Table(indexes = {
        @Index(columnList = "id"),
        @Index(columnList = "date")
})
public class TeleInfoEntity {

    @Id
    String id;
    Date date;
    Long indexHeuresCreuses;
    Long indexHeuresPleines;
    Integer kwatts;
    Integer amperes;

    TeleInfoEntity() {
        super();
    }

    public TeleInfoEntity(
            String id,
            Date date,
            Long indexHeuresCreuses,
            Long indexHeuresPleines,
            Integer kwatts,
            Integer amperes
    ) {
        this.id = id;
        this.date = date;
        this.indexHeuresCreuses = indexHeuresCreuses;
        this.indexHeuresPleines = indexHeuresPleines;
        this.kwatts = kwatts;
        this.amperes = amperes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getIndexHeuresCreuses() {
        return indexHeuresCreuses;
    }

    public void setIndexHeuresCreuses(Long indexHeuresCreuses) {
        this.indexHeuresCreuses = indexHeuresCreuses;
    }

    public Long getIndexHeuresPleines() {
        return indexHeuresPleines;
    }

    public void setIndexHeuresPleines(Long indexHeuresPleines) {
        this.indexHeuresPleines = indexHeuresPleines;
    }

    public Integer getKwatts() {
        return kwatts;
    }

    public void setKwatts(Integer kwatts) {
        this.kwatts = kwatts;
    }

    public Integer getAmperes() {
        return amperes;
    }

    public void setAmperes(Integer amperes) {
        this.amperes = amperes;
    }
}