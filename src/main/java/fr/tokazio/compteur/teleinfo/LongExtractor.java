package fr.tokazio.compteur.teleinfo;

class LongExtractor extends AbstractExtractor<Long> {

    LongExtractor(int len) {
        super(len);
    }

    /**
     * -1 if not correctly read via serial
     */
    @Override
    public Long doExtract(String str) {
        try {
            return Long.parseLong(str.trim());
        } catch (NumberFormatException ex) {
            LOGGER.warn("Error extracting long from '" + str + "'", ex);
        }
        return empty();
    }

    @Override
    public Long empty() {
        return -1l;
    }
}