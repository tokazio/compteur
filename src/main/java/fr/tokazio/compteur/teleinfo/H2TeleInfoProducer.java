package fr.tokazio.compteur.teleinfo;

import gnu.io.NoSuchPortException;
import gnu.io.UnsupportedCommOperationException;
import io.quarkus.scheduler.Scheduled;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;

@ApplicationScoped
public class H2TeleInfoProducer {

    @ConfigProperty(name = "dummy", defaultValue = "false")
    boolean dummy;

    @Inject
    Logger log;

    @Inject
    TeleInfoReader teleInfoReader;

    @Inject
    EntityManager em;

    @Transactional
    @Scheduled(every = "{every}")
    public void generate() {
        try {
            final TeleInfoData data = dummy ? DummyGenerator.generate() : teleInfoReader.readOnce();
            log.info("Saving data " + data);
            insert(data);
        } catch (IOException | UnsupportedCommOperationException | NoSuchPortException e) {
            log.error("Error getting data to send to kafka");
        }
    }

    private void insert(TeleInfoData data) {
        this.em.persist(dataToEntity(data));
    }

    private TeleInfoEntity dataToEntity(TeleInfoData data) {
        return new TeleInfoEntity(
                data.getId(),
                data.getDate(),
                data.getIndexHeuresCreuses(),
                data.getIndexHeuresPleines(),
                data.getKwatts(),
                data.getAmperes()
        );
    }
}
