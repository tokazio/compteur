package fr.tokazio.compteur.teleinfo;

class IntExtractor extends AbstractExtractor<Integer> {

    IntExtractor(int len) {
        super(len);
    }

    @Override
    public Integer doExtract(String str) {
        try {
            return Integer.parseInt(str.trim());
        } catch (NumberFormatException ex) {
            LOGGER.warn("Error extracting int from '" + str + "'", ex);
        }
        return empty();
    }

    @Override
    public Integer empty() {
        return -1;
    }
}