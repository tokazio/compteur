package fr.tokazio.compteur.teleinfo;

public enum Etiquette {

    ADCO("N° d’identification du compteur", new StringExtractor(12)),// (12 caracteres)
    OPTARIF("Option tarifaire (type d’abonnement)", new StringExtractor(4)),// (4 car.)
    ISOUSC("Intensité souscrite", new IntExtractor(2)), //( 2 car. unite = amperes)
    BASE("Index si option = base", new LongExtractor(9)), //( 9 car. unite = Wh)
    HCHC("Index heures creuses si option = heures creuses", new LongExtractor(9)),//( 9 car. unite = Wh)
    HCHP("Index heures pleines si option = heures creuses", new LongExtractor(9)),//( 9 car. unite = Wh)
    EJP_HN("Index heures normales si option = EJP", new LongExtractor(9)),//( 9 car. unite = Wh)
    EJP_HPM("Index heures de pointe mobile si option = EJP", new LongExtractor(9)),// EJP HPM ( 9 car. unite = Wh)
    BBR_HC_JB("Index heures creuses jours bleus si option = tempo :", new LongExtractor(9)),// BBR HC JB ( 9 car. unite = Wh)
    BBR_HP_JB("Index heures pleines jours bleus si option = tempo : ", new LongExtractor(9)),//BBR HP JB ( 9 car. unite = Wh)
    BBR_HC_JW("Index heures creuses jours blancs si option = tempo :", new LongExtractor(9)),// BBR HC JW ( 9 car. unite = Wh)
    BBR_HP_JW("Index heures pleines jours blancs si option = tempo :", new LongExtractor(9)),// BBR HP JW ( 9 car. unite = Wh)
    BBR_HC_JR("Index heures creuses jours rouges si option = tempo :", new LongExtractor(9)),// BBR HC JR ( 9 car. unite = Wh)
    BBR_HP_JR("Index heures pleines jours rouges si option = tempo :", new LongExtractor(9)),// BBR HP JR ( 9 car. unite = Wh)
    PEJP("Préavis EJP si option = EJP", new StringExtractor(2)),// PEJP ( 2 car.) 30mn avant periode EJP
    PTEC("Période tarifaire en cours", new StringExtractor(4)),// PTEC ( 4 car.)
    DEMAIN("Couleur du lendemain si option = tempo", new StringExtractor()),// DEMAIN
    IINST("Intensité instantanée", new IntExtractor(3)),// ( 3 car. unite = amperes)
    ADPS("Avertissement de dépassement de puissance souscrite", new IntExtractor(3)),// ( 3 car. unite = amperes) (message emis uniquement en cas de depassement effectif, dans ce cas il est immediat)
    IMAX("Intensité maximale", new IntExtractor(3)),//( 3 car. unite = amperes)
    PAPP("Puissance apparente", new IntExtractor(5)),//( 5 car. unite = Volt.amperes)
    HHPHC("Groupe horaire si option = heures creuses ou tempo", new StringExtractor(1)),// (1 car.)
    MOTDETAT("Mot d’état (autocontrôle)", new StringExtractor(6)),// (6 car.)
    TENSION("Tension", new IntExtractor(3));

    private final String desc;
    private final Extractor extractor;

    Etiquette(String desc, Extractor extractor) {
        this.desc = desc;
        this.extractor = extractor;
    }

    @Override
    public String toString() {
        return "Etiquette{" +
                "name='" + name() + '\'' +
                "desc='" + desc + '\'' +
                '}';
    }

    public String getDescription() {
        return desc;
    }

    public <T> T extract(String str) {
        return (T) extractor.extract(str);
    }
}









