package fr.tokazio.compteur.teleinfo;

import java.util.Date;
import java.util.random.RandomGenerator;

public class DummyGenerator {

    private static final RandomGenerator GEN = RandomGenerator.getDefault();

    private DummyGenerator() {
        super();
    }

    public static TeleInfoData generate() {
        final TeleInfoData data = new TeleInfoData();
        data.setDate(new Date());
        data.setIndexHeuresCreuses((long) random(1000, 10000));
        data.setIndexHeuresPleines((long) random(1000, 10000));
        data.setAmperes(random(0, 16));
        data.setKwatts(random(0, 100));
        return data;
    }

    public static int random(int from, int to) {
        return GEN.ints(from, to).findFirst().getAsInt();
    }
}
