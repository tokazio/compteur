package fr.tokazio.compteur.teleinfo;

import io.quarkus.cache.CacheResult;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.util.List;

@Path("/teleinfo")
public class TeleInfoResource {

    @Inject
    EntityManager em;

    @GET
    @CacheResult(cacheName = "teleinfo-cache")
    public List<TeleInfoEntity> list(@QueryParam("from") String from, @QueryParam("to") String to) {
        if (from == null && to == null) {
            return em.createQuery("from TeleInfoEntity order by date desc", TeleInfoEntity.class)
                    .getResultList();
        } else if (from != null && to == null) {
            return em.createQuery("from TeleInfoEntity where date >= :from order by date desc", TeleInfoEntity.class)
                    .setParameter("from", Utils.asDate(from))
                    .getResultList();
        } else if (from == null) {
            return em.createQuery("from TeleInfoEntity where date <= :to order by date desc", TeleInfoEntity.class)
                    .setParameter("to", Utils.asDate(to))
                    .getResultList();
        }
        return em.createQuery("from TeleInfoEntity where date >= :from and date <= :to order by date desc", TeleInfoEntity.class)
                .setParameter("from", Utils.asDate(from))
                .setParameter("to", Utils.asDate(to))
                .getResultList();
    }


    @GET
    @Path("/{id}")
    public List<TeleInfoEntity> list(@PathParam("id") String id) {
        final TeleInfoEntity ett = em.createQuery("from TeleInfoEntity where id = :id", TeleInfoEntity.class)
                .setParameter("id", id)
                .getSingleResult();
        if (ett != null) {
            return em.createQuery("from TeleInfoEntity where date > :date order by date desc", TeleInfoEntity.class)
                    .setParameter("date", ett.getDate())
                    .getResultList();
        }
        throw new IllegalArgumentException("Teleinfo with id " + id + " not found");
    }
}
