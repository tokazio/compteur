package fr.tokazio.compteur.teleinfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class TeleInfoData {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private final String id = UUID.randomUUID().toString();

    private Date date;

    private Long indexHeuresCreuses = -1l;

    private Long indexHeuresPleines = -1l;

    private Integer kwatts = -1;

    private Integer amperes = -1;

    public String getDateAsString() {
        return DATE_FORMAT.format(this.date);
    }

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    /**
     * never null
     * from db at read
     * new Date() at write
     */
    public void setDate(Date date) {
        this.date = date;
    }

    public Long getIndexHeuresPleines() {
        return indexHeuresPleines;
    }

    /**
     * -1 if can't be read correctly from serial
     */
    public void setIndexHeuresPleines(Long indexHeuresPleines) {
        if (indexHeuresPleines == null) {
            this.indexHeuresPleines = -1l;
        } else {
            this.indexHeuresPleines = indexHeuresPleines;
        }
    }

    public Long getIndexHeuresCreuses() {
        return indexHeuresCreuses;
    }

    /**
     * -1 if can't be read correctly from serial
     */
    public void setIndexHeuresCreuses(Long indexHeuresCreuses) {
        if (indexHeuresCreuses == null) {
            this.indexHeuresCreuses = -1l;
        } else {
            this.indexHeuresCreuses = indexHeuresCreuses;
        }
    }

    public Integer getKwatts() {
        return kwatts;
    }

    /**
     * -1 if can't be read correctly from serial
     */
    public void setKwatts(Integer kwatts) {
        if (kwatts == null) {
            this.kwatts = -1;
        } else {
            this.kwatts = kwatts;
        }
    }

    public Integer getAmperes() {
        return amperes;
    }

    /**
     * -1 if can't be read correctly from serial
     */
    public void setAmperes(Integer amperes) {
        if (amperes == null) {
            this.amperes = -1;
        } else {
            this.amperes = amperes;
        }
    }

    @Override
    public String toString() {
        return "TeleInfoData{" +
                "id=" + getId() +
                ", date=" + getDateAsString() +
                ", indexHeuresCreuses=" + getIndexHeuresCreuses() +
                ", indexHeuresPleines=" + getIndexHeuresPleines() +
                ", kwatts=" + getKwatts() +
                ", amperes=" + getAmperes() +
                '}';
    }
}
