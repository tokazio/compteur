package fr.tokazio.compteur.teleinfo;

import javax.enterprise.context.ApplicationScoped;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
class TeleInfoParser {

    public Map<Etiquette, Object> parse(List<String> strings) {
        final Map<Etiquette, Object> map = new EnumMap<>(Etiquette.class);
        for (String s : strings) {
            final String[] strs = s.split("\\s");
            if (strs.length > 0) {
                final String etiquette = s.split("\\s")[0];
                for (Etiquette e : Etiquette.values()) {
                    if (e.name().equals(etiquette)) {
                        map.put(e, e.extract(s.replace(e.name() + " ", "")));
                        break;
                    }
                }
            }
        }
        return map;
    }
}
