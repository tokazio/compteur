# compteur

Prends les données du compteur (via ttl/usb) et les mets dans une base H2

Declares itself as a zeroconf/bonjour service at application start.

Putting info in the database every minutes (see application.properties to change it)

## Service compteur

```
sudo mkdir /opt/compteur
```

```
cd /home/odroid/compteur
```

```
gradle build -Dquarkus.package.type=uber-jar -x test
```

```
sudo cp /home/odroid/compteur/build/compteur-1.0-SNAPSHOT-runner.jar /opt/compteur/compteur.jar
```

sudo nano /etc/systemd/system/compteur.service

```
[Unit]
Description=Compteur Java service

[Service]
WorkingDirectory=/opt/compteur
ExecStart=/usr/bin/java -Xmx128m -server -jar compteur.jar >> /opt/compteur/compteur.log
User=root
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```

```
sudo systemctl daemon-reload
```

```
sudo systemctl start compteur
```

## Changing configuration

create a folder /opt/compteur/conf with an application.properties file in it to override the embedded one.

```
nano src/main/resources/application.properties
gradle build -Dquarkus.package.type=uber-jar -x test
java -jar build/compteur-1.0-SNAPSHOT-runner.jar
```

## faire le odroid resize.sh

```
wget -O /usr/local/bin/odroid-utility.sh https://raw.githubusercontent.com/mdrjr/odroid-utility/master/odroid-utility.sh
chmod +x /usr/local/bin/odroid-utility.sh /usr/local/bin/odroid-utility.sh
```

## TéléInfo

Need the RxTx libraries

```
apt-get install librxtx-java
```

Maybe:

```
cp /usr/lib/jni/*.so /lib
```


## Packaging and running the application

The application can be packaged using:

```shell script
./gradlew build
```

It produces the `quarkus-run.jar` file in the `build/quarkus-app/` directory. Be aware that it’s not an _über-jar_ as
the dependencies are copied into the `build/quarkus-app/lib/` directory.

The application is now runnable using `java -jar build/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./gradlew build -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar build/*-runner.jar`.
